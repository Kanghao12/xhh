//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    openid: '',
    motto: '呀誰拉！',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  // 获取用户openid
  getOpenid: function () {
    let that = this;  //获取openid不需要授权
    wx.login({
      success: function (res) {    //请求自己后台获取用户openid
        wx.request({
          url: 'https://30paotui.com/user/wechat', data: {
            appid: 'wxc684378d9e65be69', secret: '8e78cd3355b9830a67fb94dd1e5768df', code: res.code
          }, success: function (response) {
            var openid = response.data.openid; console.log('请求获取openid:' + openid);      //可以把openid存到本地，方便以后调用
            wx.setStorageSync('openid', openid);
            that.setData({
              openid: "获取到的openid：" + openid
            })
          }
        })
      }
    })
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/gif1.gif'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
